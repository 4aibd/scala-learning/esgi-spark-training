package streaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

object SparkStreamingTP6 extends App {

  val conf = new SparkConf().setAppName("TP6").setMaster("local[*]")

  //TODO Create a SparkStreaming Context from the conf above
  val ssc = new StreamingContext(conf, Seconds(1))

  //TODO From the context, open a socketTextStream on localhost, port 9999

  // Create a DStream that will connect to hostname:port, like localhost:9999
  val lines = ssc.socketTextStream("localhost", 7077)

  //TODO Start the computation and wait for the comutation to terminate

  // Split each line into words
  val words = lines.flatMap(_.split(" "))
  // Count each word in each batch
  val pairs = words.map(word => (word, 1))
  val wordCounts = pairs.reduceByKey(_ + _)
  // Print the first ten elements of each RDD generated in this DStream to the console
  wordCounts.print()
  // Start the computation
  ssc.start()

  // Wait for the computation to terminate
  ssc.awaitTermination()

}

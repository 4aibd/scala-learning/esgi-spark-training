package streaming

import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}
import play.api.libs.json.Json

object SparkStreamingTP6IOT extends App {

  case class Device(topic: String, timestamp: String, device: String, value: Int)

  implicit val deviceReads = Json.reads[Device]

  val conf = new SparkConf().setAppName("TP6").setMaster("local[*]")

  //TODO Create a SparkStreaming Context from the conf above
  val ssc = new StreamingContext(conf, Seconds(1))

  //TODO From the context, open a socketTextStream on localhost, port 9999

  // Create a DStream that will connect to hostname:port, like localhost:9999
  val lines = ssc.socketTextStream("localhost", 9999)

  //TODO Start the computation and wait for the comutation to terminate

//   Split each line into words
//  val words = lines.map(Json.parse)
//    .flatMap(record => deviceReads.reads(record).asOpt)

  // 5.	Filtrer uniquement les messages dont la valeur du device est supérieure à 5
  val words = lines.map(Json.parse)
    .flatMap(record => deviceReads.reads(record).asOpt)
    .filter(_.value > 5)

  // Print the first ten elements of each RDD generated in this DStream to the console
  // 4.	Afficher tous les relevés de devices reçus dans le stream.
  words.print()

  // Start the computation
  ssc.start()

  // Wait for the computation to terminate
  ssc.awaitTermination()
}

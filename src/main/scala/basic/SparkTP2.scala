package basic

import org.apache.spark.sql.SparkSession

object SparkTP2 {

  def main(args: Array[String]) {

    val spark = SparkSession.builder()
      .appName("TP2")
      .config("spark.master", "local")
      .getOrCreate();

    val text = spark.sparkContext.textFile("data/spark-wikipedia.txt")

    val lineLengths = text
      .map(s => s.length)

    val totalLength = lineLengths.reduce((a, b) => a + b)

    println(totalLength)

    //TODO : Write your code here
    val appsdest = text.flatMap(line => line
      .split(" "))
      .map(p => (p, 1))
      .reduceByKey(_ + _)
      .sortBy(_._2, ascending = false) //Assuming the pair's second type has an Ordering, which is the case for Int
      .take(10)

    appsdest.foreach(println)

    val rdd = spark.sparkContext.parallelize(appsdest)
    rdd.saveAsTextFile("data/occurence.txt")

    // Accumulateur
    val accum = spark.sparkContext.longAccumulator("count-accumulator")
    text.repartition(10)
      .filter(_.contains("Spark"))
      .foreachPartition(x => accum.add(x.length))


    //Sleep to give time to browse Spark UI
    Thread.sleep(300000)
  }

}

package basic

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, desc, regexp_replace, trim}

object SparkTP3 {

  case class BabyName(year: Int, firstName: String, county: String, sex: String, count: Int)

  case class Counties(year: Int, firstName: String, county: String, sex: String, count: Int)

  def main(args: Array[String]) {

    val spark = SparkSession.builder()
      .appName("TP3")
      .config("spark.master", "local")
      .getOrCreate();

    import spark.implicits._

    val df_baby_names = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .csv("data/baby-names.csv")

    val df_counties = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .csv("data/counties.csv")

    val ds_baby_names = df_baby_names.as[BabyName]

    println(ds_baby_names.filter($"county" === "Queens").count())

    //TODO : Print the total number of girls born in 2016
    println(ds_baby_names.filter($"sex" === "F" and $"Year" === 2016).count())

    //TODO : Print the 5 most popular name of all times
    //Via dataframe
    println(ds_baby_names.groupBy("firstName")
      .sum()
      .orderBy(desc("sum(count)"))
      .show(5))

    //Via SQL
    df_baby_names.createOrReplaceTempView("baby")
    df_baby_names.sqlContext.sql("" +
      "SELECT firstName,SUM(count) as most_popular " +
      "FROM baby " +
      "GROUP BY FirstName " +
      "ORDER BY most_popular DESC").show(5)

    //TODO : Print, for each year, the number of times the First Name DAVID has been given in the state of New York
    //Via Dataframe
    //Via SQL
    df_counties.createOrReplaceTempView("counties")

    val df_county_cleaned = df_counties
      .select("county_name", "state_name")
      .withColumn("county_name",
        trim(regexp_replace(col("county_name"), " County", "")))
      .withColumn("state_name", trim(col("state_name")))
      .dropDuplicates("county_name", "state_name")

    val df_with_counties = df_baby_names.join(df_county_cleaned, df_baby_names("County") === df_county_cleaned("county_name"), "inner")
    df_with_counties.show()

    df_with_counties
      .where("state_name == 'New York' and FirstName == 'DAVID'")
      .groupBy("Year")
      .sum("Count")
      .show()


    //TODO : After the join, we're losing a lot of lines because county names don't match, how can you make sure more lines can join together?


    //Sleep to give time to browse Spark UI
    Thread.sleep(300000)
  }

}

package beersProject

import org.apache.spark.sql.SparkSession


object SparkFinalProject {
  case class Beers(id: Int,name: String, brewery_id: String, state: String, country: String, style: String,availability: String,abv: String,notes: String,retired: String)
  case class Breweries(id: Int,name: String,city: String,state: String,country: String,notes: String,types: String)
  case class Reviews( beer_id: Int,username: String,date: String,text: String,look: String,smell: String,taste: String,feel: String,overall: String,score: String  )

  def main(args: Array[String]) {

    //val path_data = "hdfs:/data/esgi-spark/final-project"
    val path_data = "data"

    val spark = SparkSession.builder()
      .appName("FinalProject")
      .config("spark.master", "local")
      .getOrCreate();

    /** COLLECT ALL DF & DS **/
    val df_beers = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(path_data + "/beers.csv")

    val df_breweries = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(path_data + "/breweries.csv")

    val df_reviews = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(path_data + "/reviews.csv")

    import spark.implicits._

    val ds_beers = df_beers.as[Beers]
    val ds_breweries = df_breweries.as[Breweries]
    val ds_reviews = df_reviews.as[Reviews]
    val textFile = spark.read.textFile(path_data + "/common-english-words.txt")
    val df_common_en = textFile.flatMap(e=>e.toLowerCase.split(","))
    /** END COLLECT ALL DF & DS **/


    print("__Spark final : Exercice 1 - la biere qui a le meilleur score\n")
    val ds_reviews_beers_Score = ds_reviews.groupBy("beer_id").avg("score").orderBy($"avg(score)".desc)
    ds_reviews_beers_Score.show(1)

    print("__Spark final : Exercice 2 - la brasserie qui a le meilleur score en moyenne sur ses bieres\n")
    val ds_beers_score = ds_beers.join(ds_reviews_beers_Score, ds_beers("id") === ds_reviews_beers_Score("beer_id"), "inner")
    ds_beers_score.groupBy("brewery_id").avg("avg(score)").orderBy($"avg(avg(score))".desc).show(1)

    print("__Spark final : Exercice 3 - les 10 pays qui ont le plus de brasserie Beer-to-go\n")
    val ds_coutry_breweries = ds_breweries.map(e=> (e.country,e.types)).filter(e =>e._2.contains("Beer-to-go"))
    ds_coutry_breweries.groupBy("_1").count().orderBy($"count".desc).show(10)

    print("__Spark final : Exercice 4 - le mot qui revient le plus dans les reviews avec les bieres IPA \n")
    val ds_join_reviews_beer = ds_reviews.join(ds_beers, ds_beers("id") === ds_reviews("beer_id"), "inner")
    val ds_reviews_word = ds_join_reviews_beer
      .filter($"style".contains("IPA"))
      .as[Reviews].map(e=>e.text)
      .flatMap(e => e.toLowerCase.split(" "))
      .map(e=> e.replaceAll("[,.!?:;)(]", ""))
      .filter(e=> e.length>1).join(df_common_en,Seq("value"),"left_anti")
      .groupBy("value")
      .count()
      .orderBy($"count".desc)
    print(ds_reviews_word.collect()(1))

    println(s"\n\nSleeping for ${args(0)}")
    Thread.sleep(args(0).toLong)
  }

}

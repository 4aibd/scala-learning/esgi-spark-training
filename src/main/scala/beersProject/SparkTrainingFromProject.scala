package beersProject

import org.apache.spark.sql.SparkSession

// id,name,brewery_id,state,country,style,availability,abv,notes,retired
// 202522,Olde Cogitator,2199,CA,US,English Oatmeal Stout, Rotating,7.3,No notes at this time.,f

// id,name,city,state,country,notes,types
// 19730,Brouwerij Danny,Erpe-Mere,,BE,No notes at this time.,Brewery

// beer_id,username,date,text,look,smell,taste,feel,overall,score

object SparkTrainingFromProject {
  case class Beers(id:Int, brewery_id:String, state:String, country:String, style:String, availability:String, abv:String, notes:String, retired:String);
  case class Breweries(id:Int, name:String, city:String, state:String, country:String, notes:String,types:String);
  case class Reviews(beer_id:Int, username:String, date:String, text:String, look:String, smell:String, taste:String, feel:String, overall:String, score:String);


  def main(args: Array[String]) {

    // val path_data = "hdfs:/data/esgi-spark/final-project"
    val path_data = "data"

    val spark = SparkSession.builder()
      .appName(getClass.getSimpleName)
      .getOrCreate()

    /** COLLECT ALL DF & DS **/
    val df_beers = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(path_data + "/beers.csv")

    val breweries_ = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(path_data + "/breweries.csv")

    val reviews_t = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .option("quote", "\"")
      .option("escape", "\"")
      .csv(path_data + "/reviews.csv")

    import spark.implicits._

    val ds_beers = df_beers.as[Beers]
    val df_breweries = breweries_.as[Breweries]
    val df_reviews = reviews_t.as[Reviews]

    // print(ds_beers.show)
    // print(df_breweries.show)
    // print(df_reviews.show)

    df_reviews.groupBy("beer_id").avg("score").orderBy($"avg(score)".desc).show(1)
  }
}

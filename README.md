# Spark Project Skeleton


Skeleton project for Spark

## Prerequisites

## Usage

##### Install

```sh
sbt compile
```

##### Packaging

```sh
sbt assembly
```

##### Test

```sh
sbt test
```

### Spark URL
 
[Scala Jobs/Stage](http://localhost:4040/jobs/)

[workers](http://localhost:8081/)

[Master](http://localhost:8080/)


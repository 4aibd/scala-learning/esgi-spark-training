package io.saagie.esgi.spark

import org.apache.spark.sql.SparkSession

object SparkTP2 {

  def main(args: Array[String]) {

    val spark = SparkSession.builder()
      .appName(getClass.getSimpleName)
      .getOrCreate()

    val text = spark.sparkContext.textFile("data/spark-wikipedia.txt",20)

    val lineLengths = text
      .map(s => s.length)

    val totalLength = lineLengths.reduce((a, b) => a + b)

    println(totalLength)

    text.flatMap(line => line.split(" "))
      .map(word => (word, 1))
      .reduceByKey(_ + _)
      .sortBy(_._2, ascending = false)
      .coalesce(1)
      .saveAsTextFile("data/results.csv")

    val accum = spark.sparkContext.longAccumulator("count-accumulator")

    text.repartition(10)
        .filter(_.contains("Spark"))
        .foreachPartition(x => accum.add(x.length))


    //Sleep to give time to browse Spark UI
    Thread.sleep(300000)
  }

}

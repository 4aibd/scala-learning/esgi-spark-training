package io.saagie.esgi.spark

import org.apache.spark.sql.functions._
import org.apache.spark.sql.{SaveMode, SparkSession}

object SparkTP3 {

  case class BabyName(year: Int, firstName: String, county: String, sex: String, count: Int)

  def main(args: Array[String]) {


    val spark = SparkSession.builder()
      .appName(getClass.getSimpleName)
      .getOrCreate()

    import spark.implicits._

    val df_baby_names = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .csv("data/baby-names.csv")

    val df_counties = spark
      .read
      .option("inferSchema", "true")
      .option("header", "true")
      .csv("data/counties.csv")

    //println(df.filter($"city" === "Queens").count())

    val ds = df_baby_names.as[BabyName]

    //TODO : Print the total number of girls born in 2016
    //NB : These 4 solutions have the same result, only the syntax is different
    println(df_baby_names.where("Sex == 'F' and Year=2016").count()) // 48991

    df_baby_names.createOrReplaceTempView("babies")
    println(spark.sql("select * from babies where year = 2016 and sex ='F'").count()) // 48991

    println(ds.filter($"Sex" === "F" && $"Year" === "2016").count()) // 48991

    println(ds.filter(_.sex == "F").filter(_.year == 2016).count()) // 48991

    //TODO : Print the 5 most popular name of all times
    ds.select("FirstName", "Count")
      .groupBy("FirstName")
      .agg(sum("Count").alias("total"))
      .sort(desc("total"))
      .show(5)


    //TODO : Print, for each year, the number of times the First Name DAVID has been given in the state of New York

    //Cleaning county name to get rid of " County"
    val df_county_cleaned = df_counties
      .select("county_name", "state_name")
      .withColumn("county_name",
        trim(regexp_replace(col("county_name"), " County", "")))
      .withColumn("state_name", trim(col("state_name")))
      .dropDuplicates("county_name", "state_name")



    //Joining initial dataset with county datasets
    val df_with_counties = df_baby_names
      .join(df_county_cleaned, df_baby_names("County") === df_county_cleaned("county_name"), "inner")

    df_with_counties.show()

    df_with_counties.where("state_name == 'New York' and FirstName == 'DAVID'")
      .groupBy("Year")
      .sum("Count")
      .show()

    //TODO : After the join, we're losing a lot of lines because county names don't match, how can you make sure more lines can join together?
    println("Total lines in baby-names dataset :" + df_baby_names.count())
    println("Total lines in baby-names dataset after join:" + df_with_counties.count())

    val optimized_join = df_baby_names
      .withColumn("county_lower", lower($"County"))
      .join(df_county_cleaned.withColumn("county_lower", lower($"county_name")),
        "county_lower")

    optimized_join.where("state_name == 'New York' and FirstName == 'DAVID'")
      .groupBy("Year")
      .sum("Count")
      .show()

    optimized_join
      .write
      .mode(SaveMode.Overwrite)
      .json("data/babies.json")

    println("Total lines in baby-names dataset after optimised join:" + optimized_join.count())

    //Sleep to give time to browse Spark UI
    Thread.sleep(300000)
  }

}
